import {Component} from '@angular/core';
import {Employee} from "./Employee";
import {EmployeeService} from "./employee.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isLoggedIn() {
    this.handleExpired();

    return localStorage.getItem('authUtils') !== null;
  }

  handleExpired() {
    const authUtils = localStorage.getItem('authUtils');
    if (authUtils && Number(JSON.parse(authUtils)._expiresIn) <= Date.now()) {
      localStorage.removeItem('authUtils');
    }
  }
}
