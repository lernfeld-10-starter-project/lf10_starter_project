export class AuthUtils {
  private _expiresIn: number | null = null;
  private _token: string = '';

  get token(): string {
    return this._token;
  }

  set token(token: string) {
    this._token = token;
  }

  get expiresIn(): number | null {
    return this._expiresIn;
  }

  set expiresIn(expiresIn: number | null) {
    this._expiresIn = expiresIn;
  }
}
