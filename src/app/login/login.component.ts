import {AfterViewInit, Component, ElementRef, Input, Output, Renderer2, ViewChild} from '@angular/core';
import {AuthUtils} from "./AuthUtils";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit {

  @Input() username: string = '';
  @Input() password: string = '';

  @Output( )error: string = '';
  authUtils: AuthUtils;

  loginForm = this.formBuilder.group({
    name: '',
    password: ''
  });

  constructor(private renderer: Renderer2 , private formBuilder: FormBuilder) {
    this.authUtils = new AuthUtils();
  }

  ngAfterViewInit(): void {
    this.renderer.setStyle(document.getElementById("body_index"), 'background-image', "url('/assets/background_login.jpg')");
    this.renderer.setStyle(document.getElementById("body_index"), 'background-repeat', "no-repeat");
    this.renderer.setStyle(document.getElementById("body_index"), 'background-size', "cover");
  }

  async onSubmit(){
    (document.getElementById("username") as HTMLInputElement).style.borderColor="";
    (document.getElementById("password") as HTMLInputElement).style.borderColor="";
    this.error='';
    try {
      const response = await fetch('/auth',{
        method:'POST',
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'grant_type=password&client_id=employee-management-service&username='+this.username+'&password='+this.password
      });
      const body = await response.json();
      this.setAuthUtils(body);
      localStorage.setItem('authUtils',JSON.stringify(this.authUtils));
    } catch (e) {
      this.error = 'Benutzername oder Passwort sind inkorrekt!'
      this.errorHandling();
    }
  }

  errorHandling(){
    (document.getElementById("username") as HTMLInputElement).style.borderColor="red";
    (document.getElementById("password") as HTMLInputElement).style.borderColor="red";
  }

  setAuthUtils(response: any) {
    this.authUtils.token = response.access_token;
    this.authUtils.expiresIn = (Date.now())+ (response.expires_in * 1000);
  }
}
