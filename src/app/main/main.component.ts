import {Component, ElementRef, HostListener, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {fromEvent, Observable, Subscription} from "rxjs";
import {Employee} from "../Employee";
import {HttpClient} from "@angular/common/http";
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @Input("search") input!:string;
  @Input("searchId") inputId!: number;

  @ViewChild('sideBar') sideBar!:ElementRef;

  employees$: Observable<Employee[]>;
  mobile: boolean = true;

  resizeObservable$!: Observable<Event>
  resizeSubscription$!: Subscription


  constructor(private http: HttpClient, private employeeService:EmployeeService) {
    this.employees$ = employeeService.getAll();
  }

  ngOnInit() {
    this.resizeObservable$ = fromEvent(window, 'resize')
    this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
      this.checkSize()
    })
  }

  checkSize(): void {
    console.log("changes")
     if(window.screen.width >= 478,9) {
       this.mobile = false;
    } else {
       this.mobile = true;
     }
  }

  deleteEmployee(id:number) {

    this.employees$ = this.employeeService.deleteById(id)
  }

  searchEmployee():void {

    this.employees$ = this.employeeService.findByInput(this.input)
  }

  searchEmployeeById(): void {
    this.employees$ = this.employeeService.findById(this.inputId)
  }

  logOut(): void {
    localStorage.removeItem('authUtils');
  }

  ngOnDestroy() {
    this.resizeSubscription$.unsubscribe()
  }


}
