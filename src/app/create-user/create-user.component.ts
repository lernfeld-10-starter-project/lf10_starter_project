import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {EmployeeService} from "../employee.service";
import {Employee} from "../Employee";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  @Input('firstName') firstName!: string;
  @Input('lastName') lastName!: string;
  @Input('street') street!: string;
  @Input('zip') zip!: string;
  @Input('city') city!: string;
  @Input('phone') phone!: string;

  submitted = false;

  constructor(private router: Router, private employeeService:EmployeeService) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    let employee = new Employee();
    employee.firstName=this.firstName;
    employee.lastName=this.lastName;
    employee.city=this.city;
    employee.postcode=this.zip;
    employee.phone=this.phone;
    employee.street=this.street;

    console.log(employee)
    this.employeeService.createEmployee(employee).subscribe(() =>this.router.navigateByUrl(""));
  }

}
