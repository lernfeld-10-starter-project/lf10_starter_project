import {Injectable} from '@angular/core';
import {catchError, filter, map, Observable, of} from "rxjs";
import {Employee} from "./Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  // @ts-ignore
  bearer = JSON.parse(localStorage.getItem('authUtils'));
  employees$: Observable<Employee[]>;

  constructor(private http: HttpClient) {
    this.employees$ = of([]);
  }

  createEmployee(employee: Employee): Observable<any> {
    return this.http.post("/backend", employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    }).pipe(
      catchError(this.handleError<any>('createEmployee'))
    )
  }

  putEmployee(employee: Employee): Observable<any> {
    return this.http.put("/backend/"+employee.id, employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    }).pipe(
      catchError(this.handleError<any>('createEmployee'))
    )
  }

  getEmployee(id:number):Observable<Employee>{
    console.log(id)
    return this.http.get<Employee>('/backend/'+id,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    }).pipe(catchError(this.handleError<Employee>('getEmployee id='+id)))

  }

  getAll() {
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    });
    return this.employees$;
  }

  deleteById(id: number) {

    this.employees$ = this.http.delete<Employee[]>('/backend/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    });
    return this.employees$;
  }

  findById(id: number) {
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    }).pipe(map(data => data.filter(employee => employee.id?.toFixed().includes(id.toString()))));
    return this.employees$;
  }

  findByInput(input: string) {
    input = input.toLowerCase();
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer._token}`)
    }).pipe(map(data => data.filter(employee => employee.firstName?.toLowerCase().includes(input) ||
      employee.lastName?.toLowerCase().includes(input) ||
      employee.city?.toLowerCase().includes(input) ||
      employee.phone?.toLowerCase().includes(input) ||
      employee.postcode?.toLowerCase().includes(input) ||
      employee.street?.toLowerCase().includes(input)
    )))
    return this.employees$;
  }

  private handleError<T>(operation: string = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    };
  }

}
