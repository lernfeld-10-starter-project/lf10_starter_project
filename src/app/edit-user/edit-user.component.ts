import {Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Employee} from "../Employee";
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  @ViewChild('firstName') firstName!:ElementRef;
  @ViewChild('lastName') lastName!:ElementRef;
  @ViewChild('street') street!:ElementRef;
  @ViewChild('postcode') postcode!:ElementRef;
  @ViewChild('city') city!:ElementRef;
  @ViewChild('phone') phone!:ElementRef;


  private id!: number;
  private sub: any;
  employee!: Employee;

  constructor(private route: ActivatedRoute, private employeeService:EmployeeService, private router: Router) {}

  async ngOnInit(): Promise<any> {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    })
    this.employeeService.getEmployee(this.id).subscribe((employeeData: Employee) => {
      this.employee = employeeData;
    });
  }

  ngAfterViewInit(){
    this.firstName.nativeElement.value = this.employee.firstName;
    this.lastName.nativeElement.value = this.employee.lastName;
    this.street.nativeElement.value = this.employee.street;
    this.postcode.nativeElement.value = this.employee.postcode;
    this.city.nativeElement.value = this.employee.city;
    this.phone.nativeElement.value = this.employee.phone;
  }

  onSubmit(){
    this.employee.firstName = this.firstName.nativeElement.value;
    this.employee.lastName = this.lastName.nativeElement.value;
    this.employee.street = this.street.nativeElement.value;
    this.employee.postcode = this.postcode.nativeElement.value;
    this.employee.city = this.city.nativeElement.value;
    this.employee.phone = this.phone.nativeElement.value;

    this.employeeService.putEmployee(this.employee).subscribe(() =>this.router.navigateByUrl(""));
  }

}
